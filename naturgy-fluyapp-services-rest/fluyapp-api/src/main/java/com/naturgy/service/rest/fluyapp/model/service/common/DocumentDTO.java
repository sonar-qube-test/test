package com.naturgy.service.rest.fluyapp.model.service.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.naturgy.service.common.model.AbstractModelInit;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class DocumentDTO.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(name = "DocumentDTO", description = "Objeto que representa el cliente")
public class DocumentDTO extends AbstractModelInit {

  private static final long serialVersionUID = 3011820849191021303L;

  private TypeDTO type;

}
