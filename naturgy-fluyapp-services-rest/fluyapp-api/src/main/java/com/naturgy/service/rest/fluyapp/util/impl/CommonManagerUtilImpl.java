package com.naturgy.service.rest.fluyapp.util.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.naturgy.service.common.model.StatusDTO;
import com.naturgy.service.rest.fluyapp.generic.client.constant.ClientCode;
import com.naturgy.service.rest.fluyapp.util.CommonManagerUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class CommonManagerUtilImpl.
 */
@Service("CommonManagerUtil")
@Slf4j
public class CommonManagerUtilImpl implements CommonManagerUtil {

  /**
   * Gets the status generic.
   *
   * @param executionCode the execution code
   * @param descriptionSuccess the description success
   * @param descriptionNotFound the description not found
   * @return the status generic
   */
  @Override
  public StatusDTO getStatusGeneric(String executionCode, String descriptionSuccess,
      String descriptionNotFound) {

    log.info("Ejecutando comando getStatusGeneric");

    StatusDTO statusGeneric = new StatusDTO();

    if (StringUtils.isNotBlank(executionCode)) {

      switch (ClientCode.valueOfCode(executionCode)) {

        case SUCCESS:

          statusGeneric.setCode(ClientCode.SUCCESS.getCode());
          statusGeneric.setDescription(descriptionSuccess);
          break;

        case NOT_FOUND_CLIENT:

          statusGeneric.setCode(ClientCode.NOT_FOUND_CLIENT.getCode());
          statusGeneric.setDescription(descriptionNotFound);
          break;

        case ERROR:

          statusGeneric.setCode(ClientCode.ERROR.getCode());
          statusGeneric.setDescription(ClientCode.ERROR.getMessage());
          break;

        case ERROR_FORMAT_SUPPLY:

          statusGeneric.setCode(ClientCode.ERROR_FORMAT_SUPPLY.getCode());
          statusGeneric.setDescription(ClientCode.ERROR_FORMAT_SUPPLY.getMessage());
          break;

        case ERROR_LOW_SUPPLY:

          statusGeneric.setCode(ClientCode.ERROR_LOW_SUPPLY.getCode());
          statusGeneric.setDescription(ClientCode.ERROR_LOW_SUPPLY.getMessage());
          break;

        default:

          log.info("Codigo no soportado");

          break;
      }
    }

    return statusGeneric;
  }

}
