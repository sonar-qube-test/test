package com.naturgy.service.rest.fluyapp.generic.client.constant;

/**
 * The Enum ClientCode.
 */
public enum ClientCode {
  
  SUCCESS("FLA00", "Cliente encontrado"), 
  NOT_FOUND_CLIENT("FLA01", "Cliente no encontrado"), 
  ERROR("FLA02", "Ejecucion con errores"),
  ERROR_FORMAT_SUPPLY("FLA03", "Formato de suministro no valido"),
  ERROR_LOW_SUPPLY("FLA04","Suministro a consultar está en estado de baja");
  
  private String code;
  private String message;

  /**
   * Instantiates a new service response code.
   *
   * @param code the code
   * @param message the message
   */
  ClientCode(String code, String message) {
    this.code = code;
    this.message = message;
  }

  /**
   * Gets the code.
   *
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * Gets the message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }
  
  /**
   * Value of code.
   *
   * @param code the code
   * @return the user authentification code
   */
  public static ClientCode valueOfCode(String code) {
    for (ClientCode e : values()) {
        if (e.getCode().equals(code)) {
            return e;
        }
    }
    return null;
}
  
}
