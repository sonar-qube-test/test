package com.naturgy.service.rest.fluyapp.generic.client;

import com.naturgy.service.rest.fluyapp.generic.client.model.GenericClientSearch;

/**
 * The Interface ClientSearchGenericService.
 */
public interface ClientSearchGenericService {

  /**
   * Client search.
   *
   * @param supplyNumber the supply number
   * @return the generic client search
   */
  GenericClientSearch clientSearch(String supplyNumber);

}
