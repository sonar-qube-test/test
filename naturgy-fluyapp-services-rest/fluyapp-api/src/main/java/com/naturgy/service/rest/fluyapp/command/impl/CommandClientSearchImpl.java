package com.naturgy.service.rest.fluyapp.command.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import com.naturgy.service.common.constant.ServiceResponseCode;
import com.naturgy.service.common.entities.NatRequestEntity;
import com.naturgy.service.common.entities.NatResponseEntity;
import com.naturgy.service.common.exception.NatBusinessException;
import com.naturgy.service.common.model.StatusDTO;
import com.naturgy.service.common.util.ResponseUtils;
import com.naturgy.service.rest.fluyapp.command.CommandClientSearch;
import com.naturgy.service.rest.fluyapp.generic.client.ClientSearchGenericService;
import com.naturgy.service.rest.fluyapp.generic.client.constant.ClientCode;
import com.naturgy.service.rest.fluyapp.generic.client.model.GenericClientSearch;
import com.naturgy.service.rest.fluyapp.generic.logging.LoggingInsertGenericService;
import com.naturgy.service.rest.fluyapp.mapper.ClientMapper;
import com.naturgy.service.rest.fluyapp.model.service.ServiceRequestClientSearchDTO;
import com.naturgy.service.rest.fluyapp.model.service.ServiceResponseClientSearchDTO;
import com.naturgy.service.rest.fluyapp.util.CommonManagerUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class CommandClientSearchImpl.
 */
@Service("CommandClientSearch")
@Slf4j
@RefreshScope
public class CommandClientSearchImpl implements CommandClientSearch {

  @Autowired
  ResponseUtils responseUtils;

  @Autowired
  private ClientSearchGenericService clientSearchGenericService;

  @Autowired
  private ClientMapper clientMapper;

  @Autowired
  private CommonManagerUtil commonManagerUtil;

  @Autowired
  private LoggingInsertGenericService loggingInsertGenericService;

  /**
   * Search.
   *
   * @param request the request
   * @return the nat response entity
   * @throws NatBusinessException the nat business exception
   */
  @Override
  @HystrixCommand(commandKey = "search", fallbackMethod = "searchFallback")
  public NatResponseEntity<ServiceResponseClientSearchDTO> search(
      final NatRequestEntity<ServiceRequestClientSearchDTO> request) throws NatBusinessException {

    log.info("Ejecutando comando search");

    final NatResponseEntity<ServiceResponseClientSearchDTO> response = new NatResponseEntity<>();

    ServiceResponseClientSearchDTO responseClient = new ServiceResponseClientSearchDTO();

    try {

      if (null != request && null != request.getBody()
          && null != request.getBody().getSupplyNumber()
          && request.getBody().getSupplyNumber().length() == 10) {

        response.setHeader(request.getHeader());

        GenericClientSearch genericClientSearch =
            clientSearchGenericService.clientSearch(request.getBody().getSupplyNumber());

        if (genericClientSearch != null) {

          responseClient
              .setClient(clientMapper.mapToClientView(genericClientSearch.getGenericClient()));
          responseClient
              .setStatus(commonManagerUtil.getStatusGeneric(genericClientSearch.getCodeExecution(),
                  ClientCode.SUCCESS.getMessage(), ClientCode.NOT_FOUND_CLIENT.getMessage()));

          if (responseClient.getStatus().getCode().equals(ClientCode.SUCCESS.getCode())) {

            loggingInsertGenericService.clientInsert(request.getBody().getSupplyNumber());

          }

        }
      } else {

        responseClient
            .setStatus(commonManagerUtil.getStatusGeneric(ClientCode.ERROR_FORMAT_SUPPLY.getCode(),
                ClientCode.SUCCESS.getMessage(), ClientCode.NOT_FOUND_CLIENT.getMessage()));

      }

      response.setStatus(new StatusDTO(ServiceResponseCode.SUCCESS));
      response.setBody(responseClient);

    } catch (Exception e) {

      response.setStatus(new StatusDTO(ServiceResponseCode.UNEXPECTED));

      log.error("Excepcion", e);

    }

    return response;

  }

  /**
   * Search fallback.
   *
   * @param request the request
   * @param throwable the throwable
   * @return the nat response entity
   * @throws NatBusinessException the nat business exception
   */
  @Override
  public NatResponseEntity<ServiceResponseClientSearchDTO> searchFallback(
      final NatRequestEntity<ServiceRequestClientSearchDTO> request, final Throwable throwable)
      throws NatBusinessException {

    log.error("El flujo de la consulta <searchFallback> se desvio al metodo fallback", throwable);

    final NatResponseEntity<ServiceResponseClientSearchDTO> response = new NatResponseEntity<>();

    response.setStatus(responseUtils.genericFallBack(request, throwable).getStatus());

    return response;

  }
}
