package com.naturgy.service.rest.fluyapp.generic.client.model;

import java.io.Serializable;
import com.naturgy.service.rest.fluyapp.generic.client.model.common.GenericClient;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class GenericClientSearch.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class GenericClientSearch implements Serializable {

  private static final long serialVersionUID = -7981757145976915711L;

  private GenericClient genericClient;
  private String codeExecution;
}
