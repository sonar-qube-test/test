package com.naturgy.service.rest.fluyapp.model.service.common;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.naturgy.service.common.model.AbstractModelInit;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class ClientDTO.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(name = "ClientDTO", description = "Objeto que representa el cliente")
public class ClientDTO extends AbstractModelInit {

  private static final long serialVersionUID = -3694237226577151175L;

  private DocumentDTO document;

  private String surName;

  private String email;

  private String telephone;

  private BigDecimal debt;

}
