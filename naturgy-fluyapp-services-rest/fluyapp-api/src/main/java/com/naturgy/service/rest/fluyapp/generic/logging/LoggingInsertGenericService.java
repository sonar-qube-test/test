package com.naturgy.service.rest.fluyapp.generic.logging;

/**
 * The Interface LoggingInsertGenericService.
 */
public interface LoggingInsertGenericService {

  /**
   * Client insert.
   *
   * @param supplyNumber the supply number
   */
  void clientInsert(String supplyNumber);

}
