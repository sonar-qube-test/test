package com.naturgy.service.rest.fluyapp.generic.client.constant;

/**
 * The Enum SearchCursorParams.
 */
public enum SearchCursorParams {
  NOMBRE_CLIENTE(0), 
  APELLIDO_CLIENTE(1), 
  CED_CLIENTE(2), 
  DOC_TIPO(3), 
  CEL_CLIENTE(4), 
  DEUDA_CLIENTE(6),
  CORREO_CLIENTE(5),
  DOC_TIPO_CODE(7),
  PROCEDURE_COD(8);

  private int param;

  /**
   * Instantiates a new search cursor params.
   *
   * @param i the i
   */
  SearchCursorParams(int i) {
    this.param = i;
  }

  /**
   * Gets the param.
   *
   * @return the param
   */
  public int getParam() {
    return param;
  }

  /**
   * Convert int to string.
   *
   * @param iParam the i param
   * @return the search cursor params
   */
  public static SearchCursorParams convertIntToString(int iParam) {
    for (SearchCursorParams param : SearchCursorParams.values()) {
      if (param.getParam() == iParam) {
        return param;
      }
    }
    return null;
  }

}
