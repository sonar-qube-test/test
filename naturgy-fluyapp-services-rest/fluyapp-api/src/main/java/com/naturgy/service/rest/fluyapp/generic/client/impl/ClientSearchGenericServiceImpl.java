package com.naturgy.service.rest.fluyapp.generic.client.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.naturgy.service.common.util.DocumentUtils;
import com.naturgy.service.rest.fluyapp.generic.client.ClientSearchGenericService;
import com.naturgy.service.rest.fluyapp.generic.client.constant.ClientCode;
import com.naturgy.service.rest.fluyapp.generic.client.constant.ClientProcedureParams;
import com.naturgy.service.rest.fluyapp.generic.client.constant.SearchCursorParams;
import com.naturgy.service.rest.fluyapp.generic.client.model.GenericClientSearch;
import com.naturgy.service.rest.fluyapp.generic.client.model.common.GenericClient;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class ClientSearchGenericServiceImpl.
 */
@Service("ClientSearchGenericService")
@Slf4j
@Transactional
public class ClientSearchGenericServiceImpl implements ClientSearchGenericService {

  @PersistenceContext()
  EntityManager entityManager;

  @Autowired
  DocumentUtils documentUtils;

  /**
   * Client search.
   *
   * @param supplyNumber the supply number
   * @return the generic client search
   */
  @SuppressWarnings("unchecked")
  @Override
  public GenericClientSearch clientSearch(String supplyNumber) {

    log.info("Ejecutando generico clientSearch");

    GenericClientSearch genericResponseToService = null;

    if (StringUtils.isNotEmpty(supplyNumber)) {

      StoredProcedureQuery clientSearchProcedure = registerParameters(supplyNumber);

      clientSearchProcedure.execute();

      genericResponseToService = validateExecution(
          String.valueOf(clientSearchProcedure
              .getOutputParameterValue(ClientProcedureParams.OUT_RESPONSE_CODE.getParam())),
          clientSearchProcedure.getResultList());

    }

    if (null == genericResponseToService) {

      genericResponseToService = new GenericClientSearch();
      genericResponseToService.setCodeExecution(ClientCode.NOT_FOUND_CLIENT.getCode());

    }

    return genericResponseToService;
  }

  /**
   * Register parameters.
   *
   * @param supplyNumber the supply number
   * @return the stored procedure query
   */
  private StoredProcedureQuery registerParameters(String supplyNumber) {

    log.info("Ejecutando metodo registerParameters");

    return entityManager
        .createStoredProcedureQuery(ClientProcedureParams.SP_CLIENT_SEARCH.getParam())
        .registerStoredProcedureParameter(ClientProcedureParams.CLIENT_NIS_SEC_IN.getParam(),
            String.class, ParameterMode.IN)
        .registerStoredProcedureParameter(ClientProcedureParams.OUT_RESPONSE_CODE.getParam(),
            String.class, ParameterMode.OUT)
        .registerStoredProcedureParameter(ClientProcedureParams.CURSOR.getParam(), Class.class,
            ParameterMode.REF_CURSOR)
        .setParameter(ClientProcedureParams.CLIENT_NIS_SEC_IN.getParam(), supplyNumber);

  }

  /**
   * Validate execution.
   *
   * @param codeExecution the code execution
   * @param genericClientObject the generic client object
   * @return the generic client search
   */
  private GenericClientSearch validateExecution(String codeExecution,
      List<Object[]> genericClientObject) {

    log.info("Ejecutando metodo validateExecution");

    GenericClientSearch genericResponseToService = new GenericClientSearch();
    GenericClient genericClient = null;

    if (codeExecution.equals(ClientCode.SUCCESS.getCode())) {

      genericResponseToService.setCodeExecution(codeExecution);

      if (!CollectionUtils.sizeIsEmpty(genericClientObject)) {

        genericClient = mapCursorToModel(genericClientObject.get(0));

        if (null != genericClient) {

          genericClient.setId(documentUtils.validateClientDocument(genericClient.getId(),
              genericClient.getDocumentTypeId()));
        }

        genericResponseToService.setGenericClient(genericClient);
      }

    } else

      genericResponseToService.setCodeExecution(codeExecution);

    return genericResponseToService;
  }

  /**
   * Map cursor to model.
   *
   * @param searchCursor the search cursor
   * @return the generic client
   */
  private GenericClient mapCursorToModel(Object[] searchCursor) {

    GenericClient clientSearch = null;

    if (null != searchCursor) {

      clientSearch = new GenericClient();

      for (int i = 0; i < searchCursor.length; i++) {

        SearchCursorParams attribute = SearchCursorParams.convertIntToString(i);

        if (searchCursor[i] != null) {

          switch (attribute) {

            case CED_CLIENTE:
              clientSearch.setId(String.valueOf(searchCursor[i]));

              break;

            case APELLIDO_CLIENTE:
              clientSearch.setSurName(String.valueOf(searchCursor[i]));

              break;
            case NOMBRE_CLIENTE:
              clientSearch.setName(String.valueOf(searchCursor[i]));

              break;

            case DOC_TIPO:
              clientSearch.setIdType(String.valueOf(searchCursor[i]));

              break;

            case CEL_CLIENTE:
              clientSearch.setTelephone(String.valueOf(searchCursor[i]));

              break;
            case CORREO_CLIENTE:
              clientSearch.setEmail(String.valueOf(searchCursor[i]));

              break;
            case DEUDA_CLIENTE:
              clientSearch.setDebt(String.valueOf(searchCursor[i]));

              break;

            case DOC_TIPO_CODE:
              clientSearch.setDocumentTypeId(String.valueOf(searchCursor[i]));

              break;

            default:
              log.info("Not found " + attribute);

          }

        }

      }
    }

    return clientSearch;
  }

}
