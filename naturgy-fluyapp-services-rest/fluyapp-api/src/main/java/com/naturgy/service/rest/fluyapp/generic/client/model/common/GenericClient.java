package com.naturgy.service.rest.fluyapp.generic.client.model.common;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class GenericClient.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class GenericClient implements Serializable {

  private static final long serialVersionUID = 4799729102567692887L;

  private String name;
  private String surName;
  private String id;
  private String idType;
  private String documentTypeId;
  private String telephone;
  private String email;
  private String debt;
}
