package com.naturgy.service.rest.fluyapp.generic.client.constant;

/**
 * The Enum ClientProcedureParams.
 */
public enum ClientProcedureParams {
  SP_CLIENT_SEARCH("GCD972.CLIENT_SEARCH_FLUYAPP"),
  CLIENT_NIS_SEC_IN("nis_sec_in"),
  CLIENT_NIS_IN("nis_rad_in"),
  CLIENT_SEC_IN("sec_nis_in"),
  CLIENT_CED_IN("cli_ced_in"),
  OUT_BLANK(""), 
  CURSOR("cursor_out"),
  OUT_RESPONSE_CODE("ps_output_code");

  private String param;

  /**
   * Instantiates a new search params.
   *
   * @param string the string
   */
  ClientProcedureParams(String string) {
    this.param = string;
  }

  /**
   * Gets the param.
   *
   * @return the param
   */
  public String getParam() {
    return param;
  }

}
