package com.naturgy.service.rest.fluyapp.generic.logging.constant;

/**
 * The Enum LoggingProcedureParams.
 */
public enum LoggingProcedureParams {
  SP_ACCESS_LOG("GCD972.ACCESS_LOG"),
  LOG_DATE_IN("access_date_in"),
  CLIENT_NIS_SEC_IN("nis_sec_in"),
  LOG_PROGRAM_IN("program_in"),
  LOG_COD_APP_IN("cod_app_in"),
  LOG_COD_FUNC_IN("cod_func_in"),
  OUT_BLANK(""), 
  OUT_RESPONSE_CODE("ps_output_code"),
  INSERT_LOG_PROGRAM("FLUYAPP"),
  INSERT_LOG_APP_CODE("FL"),
  INSERT_LOG_FUNC_CODE("FL001");

  private String param;

  /**
   * Instantiates a new search params.
   *
   * @param string the string
   */
  LoggingProcedureParams(String string) {
    this.param = string;
  }

  /**
   * Gets the param.
   *
   * @return the param
   */
  public String getParam() {
    return param;
  }

}
