package com.naturgy.service.rest.fluyapp.command;

import com.naturgy.service.common.entities.NatRequestEntity;
import com.naturgy.service.common.entities.NatResponseEntity;
import com.naturgy.service.common.exception.NatBusinessException;
import com.naturgy.service.rest.fluyapp.model.service.ServiceRequestClientSearchDTO;
import com.naturgy.service.rest.fluyapp.model.service.ServiceResponseClientSearchDTO;

/**
 * The Interface CommandClientSearch.
 */
public interface CommandClientSearch {

  /**
   * Search.
   *
   * @param request the request
   * @return the nat response entity
   * @throws NatBusinessException the nat business exception
   */
  NatResponseEntity<ServiceResponseClientSearchDTO> search(
      NatRequestEntity<ServiceRequestClientSearchDTO> request) throws NatBusinessException;

  /**
   * Search fallback.
   *
   * @param request the request
   * @param throwable the throwable
   * @return the nat response entity
   * @throws NatBusinessException the nat business exception
   */
  NatResponseEntity<ServiceResponseClientSearchDTO> searchFallback(
      NatRequestEntity<ServiceRequestClientSearchDTO> request, Throwable throwable)
      throws NatBusinessException;
}
