package com.naturgy.service.rest.fluyapp.model.service.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.naturgy.service.common.model.AbstractModelInit;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class TypeDTO.
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(name = "TypeDTO",
description = "Objeto que representa tipo")
public class TypeDTO extends AbstractModelInit {

  private static final long serialVersionUID = 5015916434627104477L;

}
