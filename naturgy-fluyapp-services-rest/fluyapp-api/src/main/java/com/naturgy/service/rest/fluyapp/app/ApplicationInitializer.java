package com.naturgy.service.rest.fluyapp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import com.naturgy.service.common.initializer.NaturgyServletInitializer;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class ApplicationInitializer.
 */
@Slf4j
@EntityScan(basePackages = {"com.naturgy.service"})
public class ApplicationInitializer extends NaturgyServletInitializer {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.naturgy.service.rest.initializer.NaturgyServletInitializer#configure(org.springframework.
   * boot.builder.SpringApplicationBuilder)
   */
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(ApplicationInitializer.class);
  }

  /**
   * The main method.
   *
   * @param args the arguments
   */
  public static void main(String[] args) {
    log.info("Iniciando aplicacion");
    SpringApplication.run(ApplicationInitializer.class, args);
  }
}
