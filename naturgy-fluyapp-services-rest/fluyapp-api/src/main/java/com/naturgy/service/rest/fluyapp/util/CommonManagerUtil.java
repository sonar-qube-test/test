package com.naturgy.service.rest.fluyapp.util;

import com.naturgy.service.common.model.StatusDTO;

/**
 * The Interface CommonManagerUtil.
 */
public interface CommonManagerUtil {

  /**
   * Gets the status generic.
   *
   * @param executionCode the execution code
   * @param descriptionSuccess the description success
   * @param descriptionNotFound the description not found
   * @return the status generic
   */
  StatusDTO getStatusGeneric(String executionCode, String descriptionSuccess,
      String descriptionNotFound);

}
