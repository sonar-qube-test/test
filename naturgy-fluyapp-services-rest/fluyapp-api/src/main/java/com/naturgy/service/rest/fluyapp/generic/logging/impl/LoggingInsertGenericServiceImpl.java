package com.naturgy.service.rest.fluyapp.generic.logging.impl;

import java.time.LocalDateTime;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.naturgy.service.rest.fluyapp.generic.logging.LoggingInsertGenericService;
import com.naturgy.service.rest.fluyapp.generic.logging.constant.LoggingProcedureParams;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class LoggingInsertGenericServiceImpl.
 */
@Service("LoggingInsertGenericServiceImpl")
@Slf4j
@Transactional
public class LoggingInsertGenericServiceImpl implements LoggingInsertGenericService {

  @PersistenceContext()
  EntityManager entityManager;

  /**
   * Client insert.
   *
   * @param supplyNumber the supply number
   */
  @Override
  public void clientInsert(String supplyNumber) {

    log.info("Ejecutando generico clientInsert");


    if (StringUtils.isNotEmpty(supplyNumber)) {

      StoredProcedureQuery clientSearchProcedure = registerParameters(supplyNumber);

      clientSearchProcedure.execute();

    }

  }

  /**
   * Register parameters.
   *
   * @param supplyNumber the supply number
   * @return the stored procedure query
   */
  private StoredProcedureQuery registerParameters(String supplyNumber) {

    log.info("Ejecutando metodo registerParameters");

    return entityManager.createStoredProcedureQuery(LoggingProcedureParams.SP_ACCESS_LOG.getParam())
        .registerStoredProcedureParameter(LoggingProcedureParams.CLIENT_NIS_SEC_IN.getParam(),
            String.class, ParameterMode.IN)
        .registerStoredProcedureParameter(LoggingProcedureParams.LOG_COD_APP_IN.getParam(),
            String.class, ParameterMode.IN)
        .registerStoredProcedureParameter(LoggingProcedureParams.LOG_PROGRAM_IN.getParam(),
            String.class, ParameterMode.IN)
        .registerStoredProcedureParameter(LoggingProcedureParams.LOG_COD_FUNC_IN.getParam(),
            String.class, ParameterMode.IN)
        .registerStoredProcedureParameter(LoggingProcedureParams.LOG_DATE_IN.getParam(),
            LocalDateTime.class, ParameterMode.IN)
        .setParameter(LoggingProcedureParams.CLIENT_NIS_SEC_IN.getParam(), supplyNumber)
        .setParameter(LoggingProcedureParams.LOG_DATE_IN.getParam(), LocalDateTime.now())
        .setParameter(LoggingProcedureParams.LOG_PROGRAM_IN.getParam(),
            LoggingProcedureParams.INSERT_LOG_PROGRAM.getParam())
        .setParameter(LoggingProcedureParams.LOG_COD_APP_IN.getParam(),
            LoggingProcedureParams.INSERT_LOG_APP_CODE.getParam())
        .setParameter(LoggingProcedureParams.LOG_COD_FUNC_IN.getParam(),
            LoggingProcedureParams.INSERT_LOG_FUNC_CODE.getParam());

  }

}
