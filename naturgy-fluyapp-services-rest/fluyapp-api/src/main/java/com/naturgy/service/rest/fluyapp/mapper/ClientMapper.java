package com.naturgy.service.rest.fluyapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import com.naturgy.service.rest.fluyapp.generic.client.model.common.GenericClient;
import com.naturgy.service.rest.fluyapp.model.service.common.ClientDTO;

/**
 * The Interface ClientMapper.
 */
@Mapper(componentModel = "spring")
public interface ClientMapper {

  /**
   * Map to client view.
   *
   * @param genericClient the generic client
   * @return the client DTO
   */
  @Mapping(target = "document.id", source = "id")
  @Mapping(target = "document.type.name", source = "idType")
  @Mapping(target = "name", source = "name")
  @Mapping(target = "surName", source = "surName")
  @Mapping(target = "email", source = "email")
  @Mapping(target = "telephone", source = "telephone")
  @Mapping(target = "debt", source = "debt")
  @Mapping(target = "id", source = "id", ignore = true)
  ClientDTO mapToClientView(GenericClient genericClient);

}
