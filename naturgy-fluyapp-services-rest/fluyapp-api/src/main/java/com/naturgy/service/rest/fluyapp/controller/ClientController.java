package com.naturgy.service.rest.fluyapp.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.naturgy.service.common.entities.NatRequestEntity;
import com.naturgy.service.common.entities.NatResponseEntity;
import com.naturgy.service.common.exception.NatBusinessException;
import com.naturgy.service.rest.fluyapp.command.CommandClientSearch;
import com.naturgy.service.rest.fluyapp.model.service.ServiceRequestClientSearchDTO;
import com.naturgy.service.rest.fluyapp.model.service.ServiceResponseClientSearchDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class ClientController.
 */
@RestController
@RequestMapping("/api/v1/client")
@Tag(name = "ClientController", description = "Manejo de la informacion de cliente")
@Slf4j
public class ClientController {

  @Autowired
  private CommandClientSearch commandClientSearch;

  /**
   * Search.
   *
   * @param request the request
   * @return the nat response entity
   * @throws NatBusinessException the nat business exception
   */
  @PostMapping(path = "/search")
  @Operation(summary = "Buscar cliente", description = "Realizar busqueda de cliente por Nis")
  public NatResponseEntity<ServiceResponseClientSearchDTO> search(
      @Valid @RequestBody NatRequestEntity<ServiceRequestClientSearchDTO> request)
      throws NatBusinessException {

    log.debug("Ejecutando servicio search");

    return commandClientSearch.search(request);
  }
}
