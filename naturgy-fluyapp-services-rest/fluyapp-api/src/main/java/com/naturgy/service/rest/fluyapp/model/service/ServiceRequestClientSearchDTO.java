package com.naturgy.service.rest.fluyapp.model.service;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class ServiceRequestClientSearchDTO.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(name = "ServiceRequestClientSearchDTO",
    description = "Objeto que representa el cuerpo de la peticion de busqueda de cliente")
public class ServiceRequestClientSearchDTO implements Serializable {
  private static final long serialVersionUID = -7344177993138793816L;

  @NotNull
  private String supplyNumber;
}
