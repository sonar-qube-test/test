package com.naturgy.service.rest.fluyapp.model.service;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.naturgy.service.common.model.StatusDTO;
import com.naturgy.service.rest.fluyapp.model.service.common.ClientDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class ServiceResponseClientSearchDTO.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(name = "ServiceResponseClientSearchDTO",
    description = "Objeto que representa el cuerpo de la respuesta de busqueda de clientes")
public class ServiceResponseClientSearchDTO implements Serializable {

  private static final long serialVersionUID = 8453122536334048510L;

  private ClientDTO client;

  private StatusDTO status;

}
